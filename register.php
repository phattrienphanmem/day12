<?php
date_default_timezone_set('Asia/Ho_Chi_Minh');
$sex = array(1 => "Nam", 0 => "Nữ");
$spec = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    p {
        margin: 0;
    }

    .wrapper {
        display: flex;
        flex-direction: column;
        width: 50%;
        margin: auto;
        margin-top: 100px;
        padding: 50px 100px;
        /* background-color: #999;
            */
        border: solid 2px #007bc7;
    }

    .form-item {
        font-size: 18px;
        width: 100%;
        display: flex;
        margin-bottom: 12px;
    }

    .form-item p {
        width: 15%;
        padding: 12px 10px 5px 10px;
        border: solid 2px #007bc7;
        background-color: #4ba3ff;
        margin-right: 15px;
        color: white;
        text-align: center;
    }

    input[type="text"] {
        width: 50%;
        border: solid 2px #007bc7;
        outline: none;
    }

    input[type="date"] {
        border: solid 2px #007bc7;
        outline: none;
    }

    input[type="radio"] {
        margin-right: 12px;
    }

    .sex {
        display: flex;
        align-items: center;
        margin-left: 10px;
    }

    select {
        border: solid 2px #007bc7;
        outline: none;
    }

    .submit-wrapper {
        justify-content: center;
        margin-top: 45px;
    }

    input[type="submit"] {
        padding: 12px 30px;
        background-color: #39bc64;
        border-radius: 10px;
        border: solid 2px #007bc7;
        cursor: pointer;
        color: white;
        font-size: 17px;
    }

    input[type="file"] {
        margin-top: 10px;
    }

    i {
        color: red;
    }

    .error {
        margin-bottom: 8px;
        font-size: 18px;
        color: red;
    }
    </style>
    <script>
    function validateForm() {
        let registerForm = document.forms['registerForm']
        const allowedExtension = ['jpeg', 'jpg', 'png', 'gif']
        let errorVal = []
        let fullNameVal = registerForm["name"].value
        let sexVal = registerForm["sex"].value
        let specVal = registerForm["spec"].value
        let birthdayVal = registerForm["date"].value
        let addressVal = registerForm["address"].value
        let image = registerForm["file"].value
        console.log(image)
        console.log(fullNameVal + "," + sexVal + "," + specVal + "," + birthdayVal + "," + image)
        if (!fullNameVal.length) {
            errorVal.push("Hãy nhập tên.")
        }
        if (sexVal === '') {
            errorVal.push("Hãy nhập giới tính.")
        }
        if (!specVal) {
            errorVal.push("Hãy nhập khoa.")
        }
        if (!birthdayVal) {
            errorVal.push("Hãy nhập ngày sinh.")
        }
        if (image) {
            let fileExt = image.split('.').pop().toLowerCase()
            if (!allowedExtension.includes(fileExt)) {
                errorVal.push("Hình ảnh phải là định dạng " + allowedExtension.join('/'))
            }
        }

        if (errorVal.length > 0) {
            let errorBox = document.getElementById('error-box')
            let errorsHtml = []
            errorBox.innerHTML = ''
            errorVal.forEach(error => {
                errorsHtml.push(`<p class="error">${error}</p>`)
            })
            errorBox.innerHTML = errorsHtml.join('')
            return false
        }

        return true
    }
    </script>
</head>

<body>
    <div class="wrapper">
        <div id="error-box">
        </div>
        <!--  -->
        <form name="registerForm" onsubmit="return validateForm()" method="POST" enctype="multipart/form-data"
            action="confirmInfo.php">
            <div class="form-item">
                <p>Họ và tên<i>*</i></p>
                <input name="name" type="text">
            </div>
            <div class="form-item">
                <p>Giới tính<i>*</i></p>
                <div class="sex">
                    <?php foreach ($sex as $key => $value) : ?>
                    <label for=<?= $key ?>>
                        <?= $value; ?>
                    </label>
                    <input id=<?= $key ?> type="radio" name="sex" value=<?= $key ?>>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="form-item">
                <p>
                    <label for="spec">
                        Phân khoa<i>*</i>
                    </label>
                </p>
                <select name="spec" id="spec">
                    <option disabled selected value></option>
                    <?php foreach ($spec as $key => $value) : ?>
                    <option value="<?= $key ?>"><?= $value ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-item">
                <p>
                    <label for="date">
                        Ngày sinh<i>*</i>
                    </label>
                </p>
                <input name="date" type="date">
            </div>
            <div class="form-item">
                <p>
                    <label for="address">
                        Địa chỉ
                    </label>
                </p>
                <input name="address" type="text" value="">
            </div>
            <div class="form-item">
                <p>
                    <label for="file">
                        Hình ảnh
                    </label>
                </p>
                <input id="file" name="file" type="file" value="">
            </div>
            <div class="form-item submit-wrapper">
                <input type="submit" value="Đăng Kí">
            </div>
        </form>
    </div>
</body>

</html>