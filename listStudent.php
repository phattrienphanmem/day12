<?php
require_once('./db/dbhelper.php');
require_once('./db/ultility.php');
$spec = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
$specName = $keyWord = NULL;
if (isset($_GET['spec'])) {
    $specName = getGET('spec');
}
if (isset($_GET['keyword'])) {
    $keyWord = getGET('keyword');
}

if ($specName != '') {
    $sql = "select * from student where faculty='$specName' and name LIKE '%$keyWord%'";
} else {
    $sql = "select * from student where name LIKE '%$keyWord%'";
}
// echo ($sql);
// echo (fixSqlInjection($sql));
$students = executeResult($sql);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List student</title>
    <style>
    body {
        font-family: sans-serif;
        padding: 10px;
    }

    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .search-form {
        display: flex;
        flex-direction: column;
        width: 40%;
        padding: 10px 30px;
    }

    .form-item p {
        padding: 12px 10px 5px 10px;
        color: black;
    }

    input[type="text"] {
        width: 100%;
        border: solid 2px #007bc7;
        outline: none;
        padding: 8px;
    }

    select {
        border: solid 2px #007bc7;
        outline: none;
        width: 100%;
        padding: 8px;
    }

    input[type="submit"],
    input[type="button"] {
        padding: 12px 20px;
        background-color: #3984bc;
        border-radius: 10px;
        border: solid 2px #007bc7;
        cursor: pointer;
        color: white;
        font-size: 17px;
    }

    table {
        width: 100%;
        border-collapse: separate;
        border-spacing: 0 1em;
    }

    td,
    th {
        /* border: 1px solid #dddddd; */
        text-align: left;
        padding: 6px;
    }

    form td {
        text-align: center;
    }

    .wrap-action {
        width: 90%;
        display: flex;
        justify-content: end;
    }

    .action {
        color: white;
        display: inline-block;
        padding: 10px;
        background-color: #3984bc;
        border: 1px solid #007bc7;
        text-decoration: none;
        border-radius: 10px;
        margin-right: 8px;
    }
    </style>
    <script>
    document.addEventListener('DOMContentLoaded', (event) => {
        const btnClear = document.getElementById('btn-clear');
        const selectSpec = document.getElementById('spec');
        const inputKeyWord = document.getElementById('key');
        btnClear.addEventListener('click', (e) => {
            console.log(inputKeyWord.value + ',' + selectSpec.value)
            selectSpec.value = ''
            inputKeyWord.value = ''
            console.log(inputKeyWord.value + ',' + selectSpec.value)
            const urlObj = new URL(location.href);
            urlObj.searchParams.delete(selectSpec.name);
            urlObj.searchParams.delete(inputKeyWord.name)
            window.history.pushState({}, '', urlObj.toString())

        })
    })
    </script>
</head>

<body>
    <div class="search-form">
        <form method="GET" enctype="multipart/form-data" action="">
            <table>
                <tr class="form-item">
                    <td>
                        <p>
                            <label for="spec">
                                Khoa
                            </label>
                        </p>
                    </td>
                    <td>
                        <select name="spec" id="spec">
                            <option value <?= !$specName ? 'selected' : '' ?>>Tất Cả</option>
                            <?php foreach ($spec as $key => $value) : ?>
                            <option value="<?= $key ?>" <?= $specName === $key ? 'selected' : '' ?>><?= $value ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="form-item">
                        <p>
                            <label for="spec">
                                Từ khóa
                            </label>
                        </p>
                    </td>
                    <td>
                        <input id="key" name="keyword" type="text" value="<?= htmlspecialchars($keyWord) ?>">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="button" value="Xóa" id="btn-clear">
                        <input type="submit" value="Tìm kiếm">
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div class="result-search">
        <span>Số sinh viên tìm thấy:</span>
        <span><?= count($students) ?></span>
    </div>
    <div class="list-student">
        <div class="wrap-action">
            <a class="action" href="./register.php">Thêm</a>
        </div>
        <div class="table-student">
            <table>
                <colgroup>
                    <col span="1" style="width: 10%;">
                    <col span="1" style="width: 25%;">
                    <col span="1" style="width: 50%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>


                <?php foreach ($students as $index => $student) : ?>
                <tr>
                    <td><?= ++$index ?></td>
                    <td><?= $student['name'] ?></td>
                    <td><?= $spec[$student['faculty']] ?></td>
                    <td>
                        <a class="action" href="">Xóa</a>
                        <a class="action" href="">Sửa</a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</body>

</html>